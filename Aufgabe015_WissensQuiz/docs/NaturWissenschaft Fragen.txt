1;Wo werden die zur Abwehr von Krankheitserreger wichtigen Lymphozyten gebildet?;Harnleiter;Knochenmark;Lymphknoten;Lungenkapillaren;B
2;Was ist wesentlicher Bestandteil des Magensaftes?;Sauerstoff;Salzs�ure;Schwefels�ure;Wasserstoff;B
3;Welcher ist der wasserreichste Fluss der Erde?;Lena;Donau;Amazonas;Nil;C
4;Wie viele Chromosomen enth�lt eine menschliche K�rperzelle in der Regel?32;24;46;64;C
5;Die Kreiszahl Pi betr�gt in etwa...?;4,122;1,129;3,141;2,341;C
6;Wie viel Paar Laufbeine hat eine Spinne?;4;8;6;2;A
7;Was produzieren Bl�tter?;Wasserstoff;Kohlenstoffdioxid & Wasser;Sauerstoff & St�rke;Sauerstoff & Schwefeldioxid;C
8;Welches Vitamin wird vom K�rper durch Sonnenlicht selbst gebildet?;Vitamin A;Vitamin B;Vitamin D;Vitamin C;C
9;Wodurch entstehen die 4 Jahreszeiten auf der Erde?;durch die Umkreisung des Mondes um die Erde;durch die Drehung der Erde um die eigene Achse;durch die schr�g stehende Erdachse;durch die Umkreisung des Mondes um die Sonne;B
10;Wie werden die dreikantigen N�sse der Buche genannt?;Bucheicheln;Buchn�sse;Kastanien;Bucheckern;D