package game;

public class User {
	
	private static String name;
	private static int punkte;
	private static int falscheAntworten=0;

	// Konstruktor um einen neuen Spieler zu erstellen
	public User(String name) {
		super();
		User.name = name;
	}

	
	// Setter/Getter
	public static String getName() {
		return name;
	}
	public static int getPunkte() {
		return punkte;
	}
	public static int getFalscheAntworten() {
		return falscheAntworten;
	}
	public static void setName(String name) {
		User.name = name;
	}
	public static void setPunkte(int punkte) {
		User.punkte = punkte;
	}
	public static void setFalscheAntworten(int falscheAntworten) {
		User.falscheAntworten = falscheAntworten;
	}	
	
	
	
	
	

}
