package game;

import java.io.*;


public class Utils {
	
	static int anzFragen = 0;
	static String[][] fragenpool; 
	BufferedReader bf=null;
	private String text;
	String zeile;
	static int counter=0;
	
	// Fragen Kategorien:
	static File geographie 	= new File ("docs/GeographieFragen.txt");
//	static File allgemein 	= new File ("docs/Allgemeinwissen Fragen.txt");
//	static File geschichte 	= new File ("docs/Geschichte Fragen.txt");
//	static File informatik 	= new File ("docs/Informatik Fragen.txt");
//	static File natur 		= new File ("docs/NaturWissenschaft Fragen.txt");
	
	// Fragen auslesen	
	
		
	
//	public static void ladeDatei() {
//		if(!geographie.canRead() || !geographie.isFile()) {
//			System.out.println("Kann nicht gelesen werden oder ist keine Datei");
//		}
//		// der ArrayListe fragenpool wird neue
//		fragenpool=new String [anzFragen][7];
//		
//		BufferedReader in = null;
//		try {
//			in = new BufferedReader(new FileReader(geographie));
//			String zeile="";
//			while ((zeile = in.readLine()) != null){
//				
//
//				
//				String[] temp = zeile.split(";");
//				
//				
//				fragenpool[counter][0] = temp [0];
//				fragenpool[counter][1] = temp [1];
//				fragenpool[counter][2] = temp [2];
//				fragenpool[counter][3] = temp [3];
//				fragenpool[counter][4] = temp [4];
//				fragenpool[counter][5] = temp [5];
//				fragenpool[counter][6] = temp [6];
//				
//				counter ++;
//				
//
//			} 
//		}catch (IOException e) {
//				e.printStackTrace();
//		}
//		try {
//			in.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//		
//		finally {
//				if(in != null) {
//				}
//			}
//		
//	}
	
	
	
	
	// Original---------------------------------------------------------------------------------------------
	public static void fragen() {
		
		// Zeilen aus der Datei lesen
		try {
			BufferedReader zeilenZaehler = new BufferedReader(new FileReader(geographie));
			try {
				while (zeilenZaehler.readLine()!=null) {
					anzFragen++;
				}
			} catch (IOException e) {
				System.out.println("Fehler beim lesen der Datei" + e.getMessage());
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			System.out.println("Fehler beim lesen der Datei. " + e.getMessage());
			e.printStackTrace();
		}
		
		
		
		// FragenPool erstellen(Zeile aus der Datei wird mit Hilfe Trennzeichen(;) getrennt angezeicht)
		
		fragenpool = new String [anzFragen][7];
		
		String aktFrage = "";
//		int counter = 0;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(geographie));
			String zeile = null;
			while ((aktFrage=br.readLine())!=null){
				
				String[] temp = aktFrage.split(";");
				
				
				fragenpool[counter][0] = temp [0];
				fragenpool[counter][1] = temp [1];
				fragenpool[counter][2] = temp [2];
				fragenpool[counter][3] = temp [3];
				fragenpool[counter][4] = temp [4];
				fragenpool[counter][5] = temp [5];
				fragenpool[counter][6] = temp [6];
				
				counter ++;
				
			}
			br.close();
			
		} catch (IOException e) {
			System.out.println("Fehler beim lesen der Datei " + e.getMessage());
			e.printStackTrace();
		}
		
		
		
	}
	


}
