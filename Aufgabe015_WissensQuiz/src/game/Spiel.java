package game;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Spiel {

	static Scanner eingabe = new Scanner(System.in);
	static int anzFragen = 0;
	static int aktFrage  = 0;
	static Random zuffi = new Random();
	static int punkte=0;
	static String ja = "ja";
	static boolean ende = false;
	
	// Eingabe vom User einer Zahl
	public static int eingabe() {
		int userEingabe = 0;
		try {
			userEingabe = eingabe.nextInt();
		} catch(InputMismatchException e){
			System.out.println("Bitte eine Zahl eingeben");
		}
		return userEingabe;
	}
	
	// Eingabe vom User ein String
	public static String eingabeAntwort() {
		return eingabe.next();
	}
	
	// Men�	
	public static void menu() {

		int userWahl;
		
		while(!ende) {
			System.out.println("Willkommen im Spiel ");
			System.out.println("Das Ziel ist so viele wie m�glich Richtige Antworten zu geben");
			System.out.println("Wenn Sie 3 Falsche Antworten geben, oder Ihr Punktestand ist 0, dann haben Sie verloren");
			System.out.println("Eine Zahl eingeben um Ihre Auswahl zu treffen");
			System.out.println("Eine Buchstage eingeben, umd die Antwort an die Frage zu geben");
			System.out.println("1. Spiel Starten");
			System.out.println("2. Spiel beenden");
			
			userWahl=eingabe();
			
			switch(userWahl) {
			case 1: 
				System.out.println("Los gehts");
				spiel();
				break;
			case 2: 
				ende=true;
				System.out.println("Sie haben Ende ausgew�llt");
				break;
			default:
			System.out.println("Bitte eine Richtige Zahl eingeben");
			}
		}
	}

	public static void spiel() {
		
		System.out.println("Momentan haben Sie  " + User.getPunkte() + " Punkte");
		Utils.fragen();

		while(Utils.anzFragen>=1) {
			if (anzFragen==1) {
				aktFrage = 0 ;
			}else {
				aktFrage=zuffi.nextInt(Utils.anzFragen-1);
			}
			
			String[] fragenSatz = Utils.fragenpool[aktFrage];
			
			System.out.println("Frage: ");
			System.out.println(fragenSatz[1]);
			System.out.println("A. " + fragenSatz[2]);
			System.out.println("B. " + fragenSatz[3]);
			System.out.println("C. " + fragenSatz[4]);
			System.out.println("D. " + fragenSatz[5]);
			System.out.println("Ihre Eingabe: ");
			
			String antwort = eingabeAntwort();
			
			if (antwort.equalsIgnoreCase(fragenSatz[6])) {
				System.out.println("Richtig");
				User.setPunkte(User.getPunkte()+1);
			}else {
				System.out.println("Leider Falsch!");
				User.setPunkte(User.getPunkte()-1);
//					ende=true;
				
				User.setFalscheAntworten(User.getFalscheAntworten()+1);
				
				if (User.getFalscheAntworten()==3) {
					System.out.println("Game Over");
					User.setFalscheAntworten(0);
					User.setPunkte(0);
					ende=true;
					
					System.out.println("Noch mal?");
					if(eingabeAntwort().equalsIgnoreCase(ja)) {
						menu();
					}else {
						ende=true;
						System.out.println("Game Over");
					}
				}
			}
			System.out.println("Punkte: " + User.getPunkte() + " Fehler: " + User.getFalscheAntworten());
			
			
		}
		
	}
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	


}
